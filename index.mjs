import express from 'express'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import cookieParser from 'cookie-parser'
import dotenv from 'dotenv'
import cors from 'cors'

dotenv.config()

const app = express()
const port = 3002

app.use(express.json())
app.use(cookieParser())
app.use(cors({origin: 'http://localhost:5173', credentials: true, exposedHeaders: ['Set-Cookie', 'Date', 'ETag']}))


const user = [
    {
        email: 'romina@ibaw.ch',
        password: '$2b$10$dzYJptEELOVhClIjc3LHSOJvwyQHaux0Dsh014/y/imTM8rRkBwua' // ASDF
    },
    {
        email: 'miro@ibaw.ch',
        password: '$2b$10$XYUZtV/67wlaYtqx.wYQAOGf2vOO7kj80A4is0LgLLhBZUdz7frfa' // ASDFG
    },
    {
        email: 'cedric@ibaw.ch',
        password: '$2b$10$XYUZtV/67wlaYtqx.wYQAOGf2vOO7kj80A4is0LgLLhBZUdz7frfa' // ASDFG
    }
]

const signup = (req, res, next) => {
    const {email, password} = req.body

    try {
        user.push({email, password: bcrypt.hash(password, 10)})
    } catch (e) {
        return res.send({success: false})
    }

    return res.send({success: true})
}

const login = (req, res, next) => {
    const {email, password} = req.body

    const foundUser = user.filter(item => item.email == email)[0]

    if (foundUser && bcrypt.compareSync(password, foundUser.password)) {
        // generate token
        const token = jwt.sign(
            {email: foundUser.email},
            process.env.TOKEN_SECRET,
            {expiresIn: '10000000s'}
        )

        // send cookie
        res.cookie('token', token, {
            sameSite: 'strict',
            httpOnly: false
        })

        return res.send({success: true})
    }
    return res.send({success: false})
}

const protectedCtrl = (req, res, next) => {
    const token = req.cookies.token
    console.log(token)

    try {
        const user = jwt.verify(token, process.env.TOKEN_SECRET)
    } catch (e) {
        return res.status(401).json({error: 'Token invalid or expired!'})
    }

    //TODO: send secret data
    return res.send({secret: 'SECRET'})
}

const logout = (req, res, next) => {
    res.clearCookie('token', {
        sameSite: 'strict',
        httpOnly: false
    })
    res.send({ success: true, message: 'Cookie deleted'});
}

app.post('/signup', signup)
app.post('/login', login)
app.get('/logout', logout)
app.get('/protected', protectedCtrl)

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})
